package Activities;

import java.util.concurrent.TimeUnit;

import org.openqa.selenium.By;
import org.openqa.selenium.JavascriptExecutor;
import org.openqa.selenium.Keys;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.firefox.FirefoxDriver;
import org.openqa.selenium.interactions.Actions;
import org.openqa.selenium.support.ui.ExpectedConditions;
import org.openqa.selenium.support.ui.WebDriverWait;
import org.testng.Assert;
import org.testng.annotations.AfterClass;
import org.testng.annotations.BeforeClass;
import org.testng.annotations.Test;

public class Activity14 {
	WebDriver driver;
	WebDriverWait wait;
	
	@Test(priority=1)
	  public void completefirsttopic() throws InterruptedException {
		//Find and click on All course menu at the header
		  driver.findElement(By.xpath("//a[@href='https://alchemy.hguy.co/lms/all-courses/']")).click();
		
		//Select Social Media Marketing course and open it.
		  JavascriptExecutor js = (JavascriptExecutor) driver; 
		  js.executeScript("window.scrollBy(0,400)");
		  
		  WebElement e1=driver.findElement(By.xpath("(//a[@class='btn btn-primary'])[1]"));
		  
		  wait.until(ExpectedConditions.visibilityOf(e1));
		  Actions builder = new Actions(driver);
		  builder.sendKeys(e1,Keys.RETURN).build().perform();
		 
		 driver.manage().timeouts().implicitlyWait(10,TimeUnit.SECONDS);
		 WebElement msg1 = driver.findElement(By.xpath("//h1[@class='entry-title']"));
		 
		 System.out.println("Title of course is " + msg1.getText());
		 
		 js.executeScript("window.scrollBy(0,1000)");
		 
		 ////Click on This is the First Topic
		  
		 WebElement firstlink =driver.findElement(By.xpath("//a[@href='https://alchemy.hguy.co/lms/topic/this-is-the-first-topic/']")); 
		 builder.sendKeys(firstlink, Keys.RETURN).build().perform();
		 
		 driver.manage().timeouts().implicitlyWait(10,TimeUnit.SECONDS);
		 
		//Mark as complete link will not be present once course is complete. So only first time below line will work and later need to comment
		  
		  //WebElement MACbutton=driver.findElement(By.xpath("(//input[@class='learndash_mark_complete_button'])[1]"));
		  //builder.sendKeys(MACbutton,Keys.RETURN).build().perform();
		  
		  driver.manage().timeouts().implicitlyWait(10,TimeUnit.SECONDS);
		 
		  WebElement status = driver.findElement(By.xpath("//div[@class='ld-status ld-status-complete ld-secondary-background']"));
		  System.out.println("Status of This is the First Topic is " + status.getText());
		
			//Click on Monitoring & Digital Advertising
			 WebElement secondlink= driver.findElement(By.xpath("//a[@href='https://alchemy.hguy.co/lms/topic/monitoring-advertising/']"));
			 driver.manage().timeouts().implicitlyWait(10,TimeUnit.SECONDS);
			 js.executeScript("window.scrollBy(0,1000)");
			 builder.sendKeys(secondlink,Keys.RETURN).build().perform();
			 
			  
			//Mark as complete link will not be present once course is complete. So only first time below line will work and later need to comment
			  
			  //builder.sendKeys(MACbutton,Keys.RETURN).build().perform();
			  
			  driver.manage().timeouts().implicitlyWait(10,TimeUnit.SECONDS);
			 
			  WebElement status1 = driver.findElement(By.xpath("//div[@class='ld-status ld-status-complete ld-secondary-background']"));
			  System.out.println("Status of course Monitoring & Digital Advertising is " + status1.getText());
			  
			//Click on Basic Investment & social Media Influencing
				 
			  WebElement thirdlink=driver.findElement(By.xpath("//a[@href='https://alchemy.hguy.co/lms/topic/basic-investment-social-media-influencing/']"));
			  driver.manage().timeouts().implicitlyWait(10,TimeUnit.SECONDS);
			  js.executeScript("window.scrollBy(0,1000)");
			  builder.sendKeys(thirdlink,Keys.RETURN).build().perform();	
			  //Mark as complete link will not be present once course is complete. So only first time below line will work and later need to comment
				
			  //builder.sendKeys(MACbutton,Keys.RETURN).build().perform();
			  
			  driver.manage().timeouts().implicitlyWait(10,TimeUnit.SECONDS);
			 
			  WebElement status2 = driver.findElement(By.xpath("//div[@class='ld-status ld-status-complete ld-secondary-background']"));
			  System.out.println("Status is course Basic Investment & social Media Influencing" + status2.getText());
	
	  }
  
	
	@Test(priority =3)
	  public void completesecondcourse() throws InterruptedException {
		  
		//Go to Home page
		  driver.findElement(By.xpath("//a[@class='ld-primary-color']")).click();
		  
		  Actions builder = new Actions(driver);
		  
		  //Navigate and expand course content of Monitoring and Digital Advertising
		  JavascriptExecutor js = (JavascriptExecutor) driver; 
		  js.executeScript("window.scrollBy(0,1000)");
		  driver.manage().timeouts().implicitlyWait(10, TimeUnit.SECONDS);
		 WebElement course =driver.findElement(By.xpath("(//span[@class='ld-text ld-primary-color'])[2]"));
		  builder.sendKeys(course,Keys.RETURN).build().perform();

		 //Click on course Success with Advert and Mark it as complete	  
		  driver.manage().timeouts().implicitlyWait(10, TimeUnit.SECONDS);
		  WebElement firstlink=driver.findElement(By.xpath("//a[@href='https://alchemy.hguy.co/lms/topic/success-with-advert/']"));
		  js.executeScript("window.scrollBy(0,1000)");
		  builder.sendKeys(firstlink,Keys.RETURN).build().perform();

		  //Mark as complete link will not be present once course is complete. So only first time below line will work and later need to comment
			
			  //WebElement MACbutton=driver.findElement(By.xpath("(//input[@class='learndash_mark_complete_button'])[1]"));
			  //wait.until(ExpectedConditions.visibilityOf(MACbutton)); 
			  //MACbutton.click();
			 	  
		  WebElement status3 = driver.findElement(By.xpath("//div[@class='ld-status ld-status-complete ld-secondary-background']"));
		  System.out.println("Status of Success with Advert is " + status3.getText());
		
		//Click and complete on Relationship course
		  driver.manage().timeouts().implicitlyWait(10, TimeUnit.SECONDS);
		  js.executeScript("window.scrollBy(0,1000)");
		  
		  WebElement secondlink=driver.findElement(By.xpath("(//a[@href='https://alchemy.hguy.co/lms/topic/relationships/'])[1]"));
		  builder.sendKeys(secondlink,Keys.RETURN).build().perform();
			
			  //Mark as complete link will not be present once course is complete. So only first time below line will work and later need to comment
		 // wait.until(ExpectedConditions.visibilityOf(MACbutton));   
		  //MACbutton.click();
			 
		
		  WebElement status4 = driver.findElement(By.xpath("//div[@class='ld-status ld-status-complete ld-secondary-background']"));
		  System.out.println("Status is of Relationship course is " + status4.getText());

	  }	
	@Test(priority=4)
	  public void completethirdcourse() throws InterruptedException {
		  
		//Go to Home page
		  driver.findElement(By.xpath("//a[@class='ld-primary-color']")).click();
		  
		  Actions builder = new Actions(driver);
			  
			  //Navigate and expand course content of Monitoring and Digital Advertising
			  JavascriptExecutor js = (JavascriptExecutor) driver; 
			  js.executeScript("window.scrollBy(0,500)");
			  driver.manage().timeouts().implicitlyWait(10, TimeUnit.SECONDS);
			  //Complete Investment & Marketing Final Strategies course
		 
			 WebElement course =driver.findElement(By.xpath("(//div[@class='ld-item-title'])[3]"));
			  builder.sendKeys(course,Keys.RETURN).build().perform();
		
		  	 System.out.println("Course name is" + course.getText());
			
			   
			  //Click on Mark as Complete Link
			
			  WebElement MACbutton=driver.findElement(By.xpath("(//input[@class='learndash_mark_complete_button'])[1]"));
			  wait.until(ExpectedConditions.visibilityOf(MACbutton)); 
			  MACbutton.click();
			 		
			 WebElement status5 = driver.findElement(By.xpath("//div[@class='ld-status ld-status-complete ld-secondary-background']")); 
			 System.out.println("Status of Investment & Marketing Final Strategies course is  " + status5.getText());
	
	}

@BeforeClass
	public void beforeClass() {
	  //initialization of the driver
		driver = new FirefoxDriver();
		 //Open the URL
		driver.get("https://alchemy.hguy.co/lms/all-courses/");
		driver.manage().window().maximize();
		wait = new WebDriverWait(driver, 20);
	
	}

	@AfterClass
	public void afterClass() {
		//Close the browser
		driver.close();
	}

@Test(priority=0)
public void Login() {
	  driver.findElement(By.xpath("//a[contains(text(),'My Account')]")).click();
		wait.until(ExpectedConditions.visibilityOf(driver.findElement(By.xpath("//h1[@class='uagb-ifb-title']"))));
		driver.findElement(By.xpath("//a[@class='ld-login ld-login ld-login-text ld-login-button ld-button']")).click();
		
		driver.findElement(By.xpath("//input[@id='user_login']")).sendKeys("root");
		
		driver.findElement(By.xpath("//input[@id='user_pass']")).sendKeys("pa$$w0rd");
		driver.findElement(By.xpath("//input[@id='wp-submit']")).click();
		
		String header= driver.findElement(By.xpath("//a[contains(text(),'Howdy')]")).getText();
		System.out.println(header);
		String expect= "Howdy, root";
		Assert.assertEquals(header, expect);
	  
	  
}
  
  
  
  
}
