package Activities;

import java.util.List;

import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.firefox.FirefoxDriver;
import org.openqa.selenium.support.ui.WebDriverWait;
import org.testng.annotations.AfterClass;
import org.testng.annotations.BeforeClass;
import org.testng.annotations.Test;

public class Activity7 {
	WebDriver driver;
	WebDriverWait wait;
  @Test
  public void Testcase7() {
	  //Find and click on All course menu at the header
	  driver.findElement(By.xpath("//a[@href='https://alchemy.hguy.co/lms/all-courses/']")).click();
	  //find list of courses provided
	  List<WebElement> list=driver.findElements(By.xpath("//h3[@class='entry-title']"));
	  //Print the total number of courses
	  System.out.println("Courses provided are " + list.size());
  }
  
  @BeforeClass
	public void beforeClass() {//initialization of the driver
		driver = new FirefoxDriver();
		 //Open the URL
		driver.get("https://alchemy.hguy.co/lms");
		driver.manage().window().maximize();
		wait = new WebDriverWait(driver, 20);
	}

	@AfterClass
	public void afterClass() {
		//Close the browser
		driver.close();
	}
 


}
