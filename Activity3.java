package Activities;

import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.firefox.FirefoxDriver;
import org.openqa.selenium.support.ui.ExpectedConditions;
import org.openqa.selenium.support.ui.WebDriverWait;
import org.testng.Assert;
import org.testng.annotations.AfterClass;
import org.testng.annotations.BeforeClass;
import org.testng.annotations.Test;

public class Activity3 {

	WebDriver driver;
	WebDriverWait wait;
  @Test
  public void Testcase3() throws InterruptedException {
	  wait.until(ExpectedConditions.visibilityOf(driver.findElement(By.xpath("//h1[@class='uagb-ifb-title']"))));
	  
	  Thread.sleep(5000);
	  String heading1 = driver.findElement(By.xpath("(//h3[@class='uagb-ifb-title'])[1]")).getText();
	  String exepected="Actionable Training";
	  System.out.println("Title of the page is " + heading1);
	  Assert.assertEquals(heading1, exepected);
  }
  
  @BeforeClass
  public void beforeClass() {
	  
	  driver = new FirefoxDriver();
	  driver.get("https://alchemy.hguy.co/lms");
	  driver.manage().window().maximize();
	  wait = new WebDriverWait(driver,20);
  }

  @AfterClass
  public void afterClass() {
	  
	  driver.close();
  }
  
}
