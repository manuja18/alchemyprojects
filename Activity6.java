package Activities;

import static org.testng.Assert.assertEquals;

import org.openqa.selenium.Alert;
import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.firefox.FirefoxDriver;
import org.openqa.selenium.support.ui.ExpectedConditions;
import org.openqa.selenium.support.ui.WebDriverWait;
import org.testng.Assert;
import org.testng.annotations.AfterClass;
import org.testng.annotations.BeforeClass;
import org.testng.annotations.Test;

public class Activity6 {
	WebDriver driver;
	WebDriverWait wait;

	@Test
	public void Testcase6() throws InterruptedException {
		driver.findElement(By.xpath("//a[contains(text(),'My Account')]")).click();
		wait.until(ExpectedConditions.visibilityOf(driver.findElement(By.xpath("//h1[@class='uagb-ifb-title']"))));
		driver.findElement(By.xpath("//a[@class='ld-login ld-login ld-login-text ld-login-button ld-button']")).click();
		
		driver.findElement(By.xpath("//input[@id='user_login']")).sendKeys("root");
		
		driver.findElement(By.xpath("//input[@id='user_pass']")).sendKeys("pa$$w0rd");
		driver.findElement(By.xpath("//input[@id='wp-submit']")).click();
		
		String header= driver.findElement(By.xpath("//a[contains(text(),'Howdy')]")).getText();
		System.out.println(header);
		String expect= "Howdy, root";
		Assert.assertEquals(header, expect);
		 

	}

	@BeforeClass
	public void beforeClass() {

		driver = new FirefoxDriver();
		driver.get("https://alchemy.hguy.co/lms");
		driver.manage().window().maximize();
		wait = new WebDriverWait(driver, 20);
		
	}

	@AfterClass
	public void afterClass() {

		driver.close();
	}

}
