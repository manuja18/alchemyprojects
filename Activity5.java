package Activities;

import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.firefox.FirefoxDriver;
import org.openqa.selenium.support.ui.ExpectedConditions;
import org.openqa.selenium.support.ui.WebDriverWait;
import org.testng.Assert;
import org.testng.annotations.AfterClass;
import org.testng.annotations.BeforeClass;
import org.testng.annotations.Test;

public class Activity5 {
	WebDriver driver;
	WebDriverWait wait;
	
  @Test
  public void Testcase5() {
	  driver.findElement(By.xpath("//a[contains(text(),'My Account')]")).click();
	  wait.until(ExpectedConditions.visibilityOf(driver.findElement(By.xpath("//h1[@class='uagb-ifb-title']"))));
	  String expected ="My Account � Alchemy LMS";
	  
	String Actual=driver.getTitle();
	Assert.assertEquals(Actual, expected);
	  
	  
  }
  
  @BeforeClass
  public void beforeClass() {
	  
	  driver = new FirefoxDriver();
	  driver.get("https://alchemy.hguy.co/lms");
	  driver.manage().window().maximize();
	  wait = new WebDriverWait(driver,20);
  }

  @AfterClass
  public void afterClass() {
	  
	  driver.close();
  }
  
  
  
  
}
