package Activities;

import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.firefox.FirefoxDriver;
import org.openqa.selenium.support.ui.WebDriverWait;
import org.testng.Assert;
import org.testng.annotations.AfterClass;
import org.testng.annotations.BeforeClass;
import org.testng.annotations.Test;

public class Activity4 {
	WebDriver driver;
	WebDriverWait wait;
  @Test
  public void Testcase4() {
	  String actual= driver.findElement(By.xpath("(//h3[@class='entry-title'])[2]")).getText();
	  String expected="Email Marketing Strategies";
	  Assert.assertEquals(actual, expected);
	  System.out.println(actual);
	  	  
  }
  @BeforeClass
  public void beforeClass() {
	  
	  driver = new FirefoxDriver();
	  driver.get("https://alchemy.hguy.co/lms");
	  driver.manage().window().maximize();
	  wait = new WebDriverWait(driver,20);
  }

  @AfterClass
  public void afterClass() {
	  
	  driver.close();
  }
  

}
