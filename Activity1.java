package Activities;

import static org.testng.Assert.assertEquals;

import org.openqa.selenium.WebDriver;
import org.openqa.selenium.firefox.FirefoxDriver;
import org.testng.annotations.AfterClass;
import org.testng.annotations.BeforeClass;
import org.testng.annotations.Test;

public class Activity1 {
	WebDriver driver;
	  @Test
	  public void testcase1() {
		  
		  String title = driver.getTitle();
		  String expected="Alchemy LMS � An LMS Application";
		  assertEquals(title, expected);
	  }
	  @BeforeClass
	  public void beforeClass() {
		  
		  driver = new FirefoxDriver();
		  driver.get("https://alchemy.hguy.co/lms");
		  driver.manage().window().maximize();
	  }

	  @AfterClass
	  public void afterClass() {
		  
		  driver.close();
	  }
}
