package Activities;

import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.firefox.FirefoxDriver;
import org.openqa.selenium.support.ui.WebDriverWait;
import org.testng.annotations.AfterClass;
import org.testng.annotations.BeforeClass;
import org.testng.annotations.Test;

public class Activity8 {
	WebDriver driver;
	WebDriverWait wait;
  @Test
  public void testcase8() throws InterruptedException {
	  
	 //Click on Contacts link 
	 driver.findElement(By.xpath("//a[@href='https://alchemy.hguy.co/lms/contact/']")).click();
	 //Enter the details on form
	 driver.findElement(By.xpath("//input[@id='wpforms-8-field_0']")).sendKeys("Manuja");
	 driver.findElement(By.xpath("//input[@id='wpforms-8-field_1']")).sendKeys("mankshir@in.ibm.com");
	 driver.findElement(By.xpath("//textarea[@id='wpforms-8-field_2']")).sendKeys("This is test message");
	 
	 //Click on Submit button
	 driver.findElement(By.xpath("//button[@id='wpforms-submit-8']")).click();
	 //Read the confirmation message
	 String expectedmsg=driver.findElement(By.xpath("//div[@id='wpforms-confirmation-8']")).getText();
	 //Print the confirmation message
	 System.out.println("Confirmation message is " +expectedmsg);
	
  }
  
  
  @BeforeClass
 	public void beforeClass() {
	  //initialization of the driver
 		driver = new FirefoxDriver();
 		 //Open the URL
 		driver.get("https://alchemy.hguy.co/lms");
 		driver.manage().window().maximize();
 		wait = new WebDriverWait(driver, 20);
 	}

 	@AfterClass
 	public void afterClass() {
 		//Close the browser
 		driver.close();
 	}
  
}
