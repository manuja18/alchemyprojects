package Activities;

import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.firefox.FirefoxDriver;
import org.openqa.selenium.support.ui.ExpectedConditions;
import org.openqa.selenium.support.ui.WebDriverWait;
import org.testng.Assert;
import org.testng.annotations.AfterClass;
import org.testng.annotations.BeforeClass;
import org.testng.annotations.Test;

public class Activity13 {
	WebDriver driver;
	WebDriverWait wait;
  @Test(priority=1)
  public void Testcase11() throws InterruptedException {
	  
	//Find and click on All course menu at the header
	  driver.findElement(By.xpath("//a[@href='https://alchemy.hguy.co/lms/all-courses/']")).click();
	
	  //Print the title of the page
	  System.out.println("Title of course is " +driver.findElement(By.xpath("(//h3[@class='entry-title'])[1]")).getText() );
	
	//Select Social Media Marketing course and open it.
	  driver.findElement(By.xpath("(//a[@class='btn btn-primary'])[1]")).click(); 

  }
  
  @Test(priority=2)
  public void completefirsttopic() {
	  
	  //Click on Expand for Developing Strategy topic
	  driver.findElement(By.xpath("(//span[@class='ld-text ld-primary-color'])[1]")).click();
	  //Click on This is the First Topic
	  driver.findElement(By.xpath("//a[@href='https://alchemy.hguy.co/lms/topic/this-is-the-first-topic/']")).click();
	  //Mark as complete link will not be present once course is complete. So only first time below line will work and later need to comment
	  
		  WebElement MACbutton=driver.findElement(By.xpath("(//input[@class='learndash_mark_complete_button'])[1]"));
		  wait.until(ExpectedConditions.visibilityOf(MACbutton)); 
		  MACbutton.click();
		 
	  WebElement status = driver.findElement(By.xpath("//div[@class='ld-status ld-status-complete ld-secondary-background']"));
	  System.out.println("Status of This is the First Topic is " + status.getText());
	  
	  driver.findElement(By.xpath("//a[@class='ld-primary-color']")).click();
	  
	//Click on Monitoring & Digital Advertising
	  driver.findElement(By.xpath("//a[@href='https://alchemy.hguy.co/lms/topic/monitoring-advertising/']")).click();
	  //Mark as complete link will not be present once course is complete. So only first time below line will work and later need to comment
		
		  
		  wait.until(ExpectedConditions.visibilityOf(MACbutton)); 
		  MACbutton.click();
		 
	  
	  WebElement status1 = driver.findElement(By.xpath("//div[@class='ld-status ld-status-complete ld-secondary-background']"));
	  System.out.println("Status of course Monitoring & Digital Advertising is " + status1.getText());
	 
	  driver.findElement(By.xpath("//a[@class='ld-primary-color']")).click();
	//Click on Basic Investment & social Media Influencing
	  driver.findElement(By.xpath("//a[@href='https://alchemy.hguy.co/lms/topic/basic-investment-social-media-influencing/']")).click();
	  //Mark as complete link will not be present once course is complete. So only first time below line will work and later need to comment
		
		  wait.until(ExpectedConditions.visibilityOf(MACbutton));
		  MACbutton.click();
		 
	  
	
	  WebElement status2 = driver.findElement(By.xpath("//div[@class='ld-status ld-status-complete ld-secondary-background']"));
	  System.out.println("Status is course Basic Investment & social Media Influencing" + status2.getText());
	
  }
  
  @Test(priority =3)
  public void completesecondcourse() throws InterruptedException {
	  
	//Go to Home page
	  driver.findElement(By.xpath("//a[@class='ld-primary-color']")).click();
	  driver.findElement(By.xpath("//a[@class='ld-primary-color']")).click();
	  
	  //Navigate and expand course content of Monitoring and Digital Advertising
	  driver.findElement(By.xpath("(//span[@class='ld-text ld-primary-color'])[2]")).click();
	  
	 //Click on course Success with Advert and Mark it as complete
	  
	 
	  driver.findElement(By.xpath("//a[@href='https://alchemy.hguy.co/lms/topic/success-with-advert/']")).click();
	  

	  //Mark as complete link will not be present once course is complete. So only first time below line will work and later need to comment
		
		  WebElement MACbutton=driver.findElement(By.xpath("(//input[@class='learndash_mark_complete_button'])[1]"));
		  wait.until(ExpectedConditions.visibilityOf(MACbutton)); 
		  MACbutton.click();
		 	  
	  WebElement status3 = driver.findElement(By.xpath("//div[@class='ld-status ld-status-complete ld-secondary-background']"));
	  System.out.println("Status of Success with Advert is " + status3.getText());
	  
	//Click on Back to lesson link
	  driver.findElement(By.xpath("//a[@class='ld-primary-color']")).click();
	  
	  
	//Click and complete on Relationship course
	  driver.findElement(By.xpath("(//a[@href='https://alchemy.hguy.co/lms/topic/relationships/'])[1]")).click();
		
		  //Mark as complete link will not be present once course is complete. So only first time below line will work and later need to comment
	  wait.until(ExpectedConditions.visibilityOf(MACbutton));   
	  MACbutton.click();
		 
	
	  WebElement status4 = driver.findElement(By.xpath("//div[@class='ld-status ld-status-complete ld-secondary-background']"));
	  System.out.println("Status is of Relationship course is " + status4.getText());

  }
  
  @Test(priority=4)
  public void completethirdcourse() throws InterruptedException {
	  
	//Go to Home page
	  driver.findElement(By.xpath("//a[@class='ld-primary-color']")).click();
	  driver.findElement(By.xpath("//a[@class='ld-primary-color']")).click();
	  
	  //Complete Investment & Marketing Final Strategies course
	 
	  WebElement thirdLink= driver.findElement(By.xpath("(//div[@class='ld-item-title'])[3]"));
	  System.out.println("Course name is" + thirdLink.getText());
		
		   thirdLink.click(); 
		  //Click on Mark as Complete Link
		
		  WebElement MACbutton=driver.findElement(By.xpath("(//input[@class='learndash_mark_complete_button'])[1]"));
		  wait.until(ExpectedConditions.visibilityOf(MACbutton)); 
		  MACbutton.click();
		 		
		 WebElement status5 = driver.findElement(By.xpath("//div[@class='ld-status ld-status-complete ld-secondary-background']")); 
		 System.out.println("Status of Investment & Marketing Final Strategies course is  " + status5.getText());
	  
  }
  
  @Test(priority=5)
	
	public void completionstatus() {
	  
	        //navigate to home page
			driver.findElement(By.xpath("//a[@class='ld-primary-color']")).click();
			  driver.findElement(By.xpath("//a[@class='ld-primary-color']")).click();
		//Find and click on All course menu at the header
		  driver.findElement(By.xpath("//a[@href='https://alchemy.hguy.co/lms/all-courses/']")).click();
		
		  //Print the title of the page
		  System.out.println("Title of course is " +driver.findElement(By.xpath("(//h3[@class='entry-title'])[1]")).getText() );
		
		//Select Social Media Marketing course and open it.
		  driver.findElement(By.xpath("(//a[@class='btn btn-primary'])[1]")).click(); 
		 //Find the completion percentage
		  
		  String percentage = driver.findElement(By.xpath("(//div[@class='ld-progress-percentage ld-secondary-color'])[1]")).getText();
		  System.out.println("Course percentage is " + percentage);
		
		
	}
  
  @BeforeClass
	public void beforeClass() {
	  //initialization of the driver
		driver = new FirefoxDriver();
		 //Open the URL
		driver.get("https://alchemy.hguy.co/lms");
		driver.manage().window().maximize();
		wait = new WebDriverWait(driver, 20);
	
	}

	@AfterClass
	public void afterClass() {
		//Close the browser
		driver.close();
	}

@Test(priority=0)
public void Login() {
	  driver.findElement(By.xpath("//a[contains(text(),'My Account')]")).click();
		wait.until(ExpectedConditions.visibilityOf(driver.findElement(By.xpath("//h1[@class='uagb-ifb-title']"))));
		driver.findElement(By.xpath("//a[@class='ld-login ld-login ld-login-text ld-login-button ld-button']")).click();
		
		driver.findElement(By.xpath("//input[@id='user_login']")).sendKeys("root");
		
		driver.findElement(By.xpath("//input[@id='user_pass']")).sendKeys("pa$$w0rd");
		driver.findElement(By.xpath("//input[@id='wp-submit']")).click();
		
		String header= driver.findElement(By.xpath("//a[contains(text(),'Howdy')]")).getText();
		System.out.println(header);
		String expect= "Howdy, root";
		Assert.assertEquals(header, expect);
	  
	  
}

}
