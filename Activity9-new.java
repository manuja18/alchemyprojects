package Activities;

import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.firefox.FirefoxDriver;
import org.openqa.selenium.support.ui.ExpectedConditions;
import org.openqa.selenium.support.ui.WebDriverWait;
import org.testng.annotations.AfterClass;
import org.testng.annotations.BeforeClass;
import org.testng.annotations.Test;

public class Activity9 {
	WebDriver driver;
	WebDriverWait wait;
  @Test
  public void Testcase9() {
	  
	 
	//Find and click on All course menu at the header
	  driver.findElement(By.xpath("//a[@href='https://alchemy.hguy.co/lms/all-courses/']")).click();
	
	  //Print the title of the page
	  System.out.println("Title of course is " +driver.findElement(By.xpath("(//h3[@class='entry-title'])[1]")).getText() );
	  
	  //Select any course and open it.
	  driver.findElement(By.xpath("(//a[@class='btn btn-primary'])[1]")).click();
	  
	  //Navigate and expand course content for monitoring & Digital Advertising
	  driver.findElement(By.xpath("(//span[@class='ld-text ld-primary-color'])[2]")).click();
	  
	 //Click on Success with Advert topic radio button and Mark it as complete
	  
	  WebElement forthlink = driver.findElement(By.xpath("(//a[@href='https://alchemy.hguy.co/lms/topic/success-with-advert/']"));
	  wait.until(ExpectedConditions.visibilityOf(forthlink));
	 
	  System.out.println("Print " + forthlink.getText());
	  driver.findElement(By.xpath("//a[@href='https://alchemy.hguy.co/lms/topic/success-with-advert/']")).click();

	  //Mark as complete link will not be present once course is complete. So only first time below line will work and later need to comment
	  //driver.findElement(By.xpath("(//input[@class='learndash_mark_complete_button'])[1]")).click();
	  
	
	  WebElement status = driver.findElement(By.xpath("//div[@class='ld-status ld-status-complete ld-secondary-background']"));
	  System.out.println("Status is " + status.getText());
  }
  
  @BeforeClass
	public void beforeClass() {
	  //initialization of the driver
		driver = new FirefoxDriver();
		 //Open the URL
		driver.get("https://alchemy.hguy.co/lms");
		driver.manage().window().maximize();
		wait = new WebDriverWait(driver, 20);
	
	}

	@AfterClass
	public void afterClass() {
		//Close the browser
		driver.close();
	}

  
  
}
