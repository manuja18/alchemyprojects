package Activities;

import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.firefox.FirefoxDriver;
import org.openqa.selenium.support.ui.ExpectedConditions;
import org.openqa.selenium.support.ui.WebDriverWait;
import org.testng.Assert;
import org.testng.annotations.AfterClass;
import org.testng.annotations.BeforeClass;
import org.testng.annotations.Test;

public class Activity12 {
	WebDriver driver;
	WebDriverWait wait;
	@Test
	
	public void completionstatus() {
		//Find and click on All course menu at the header
		  driver.findElement(By.xpath("//a[@href='https://alchemy.hguy.co/lms/all-courses/']")).click();
		
		  //Print the title of the page
		  System.out.println("Title of course is " +driver.findElement(By.xpath("(//h3[@class='entry-title'])[1]")).getText() );
		
		//Select Social Media Marketing course and open it.
		  driver.findElement(By.xpath("(//a[@class='btn btn-primary'])[1]")).click(); 
		 //Find the completion percentage
		  
		  String percentage = driver.findElement(By.xpath("(//div[@class='ld-progress-percentage ld-secondary-color'])[1]")).getText();
		  System.out.println("Course percentage is " + percentage);
		
		
	}
	
	 @BeforeClass
		public void beforeClass() {
		  //initialization of the driver
			driver = new FirefoxDriver();
			 //Open the URL
			driver.get("https://alchemy.hguy.co/lms");
			driver.manage().window().maximize();
			wait = new WebDriverWait(driver, 20);
		
		}

		@AfterClass
		public void afterClass() {
			//Close the browser
			driver.close();
		}

	@Test(priority=0)
	public void Login() {
		  driver.findElement(By.xpath("//a[contains(text(),'My Account')]")).click();
			wait.until(ExpectedConditions.visibilityOf(driver.findElement(By.xpath("//h1[@class='uagb-ifb-title']"))));
			driver.findElement(By.xpath("//a[@class='ld-login ld-login ld-login-text ld-login-button ld-button']")).click();
			
			driver.findElement(By.xpath("//input[@id='user_login']")).sendKeys("root");
			
			driver.findElement(By.xpath("//input[@id='user_pass']")).sendKeys("pa$$w0rd");
			driver.findElement(By.xpath("//input[@id='wp-submit']")).click();
			
			String header= driver.findElement(By.xpath("//a[contains(text(),'Howdy')]")).getText();
			System.out.println(header);
			String expect= "Howdy, root";
			Assert.assertEquals(header, expect);
		  
		  
	}

}
